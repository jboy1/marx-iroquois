---
title: Karl Marx and the Iroquois
author: Franklin Rosemont
---

[^publication-note] There are works that come down to us with question marks
blazing like sawed-off shotguns, scattering here and there and everywhere
sparks that illuminate our own restless search for answers. Ralegh's so-called
*Cynthia* cycle, Sade's *120 Days*, Fourier's *New Amorous World*,
Lautréamont's *Poésies*, Lenin's *Notes on Hegel*, Randolph Bourne's essay on
"The State," Jacques Vaché's *War Letters*, Duchamp's *Green Box*, the Samuel
Greenberg manuscripts: These are only a few of the extraordinary *fragments*
that have, for many of us, exerted a fascination greater than that of all but a
very few "finished" works.

[^publication-note]:
  {-} Originally published in *Arsenal/Surrealist Subversion*, vol. 4 (Chicago:
      Black Swan Press, 1989).

[^notebooks] Karl Marx's *Ethnological Notebooks*---notes for a major study he
never lived to write---have something of the same fugitive ambiguity. These
extensively annotated excerpts from works of Lewis Henry Morgan and others are
a jigsaw puzzle for which we have to reinvent the missing pieces out of our own
research and reverie and above all, our own revolutionary activity. Typically,
although the existence of the notebooks has been known since Marx's death in
1883, they were published integrally for the first time only eighty-nine years
later, and then only in a highly priced edition aimed at specialists. A
transcription of text exactly as Marx wrote it, the book presents the reader
with all the difficulties of *Finnegan's Wake* and more, with its curious
mixture of English, German, French, Latin and Greek, and a smattering of words
and phrases from many non-European languages, from Ojibwa to Sanskrit. Cryptic
shorthand abbreviations, incomplete and run-on sentences, interpolated
exclamations, erudite allusions to classical mythology, passing references to
contemporary world affairs, generous doses of slang and vulgarity, irony and
invective: All these the volume possesses aplenty, and they are not the
ingredients of smooth reading. This is not a work of which it can be said,
simply, that it was "not prepared by the author for publication"; indeed, it is
very far from being even a "rough draft." Rather it is the raw substance of a
work, a private jumble of jottings intended for no other eyes than Marx's
own---the spontaneous record of his "conversations" with the authors he was
reading, with other authors whom they quoted, and, finally and especially, with
himself. In view of the fact that Marx's clearest, most refined texts have
provoked so many contradictory interpretations, it is perhaps not so strange
that his devoted students, seeking the most effective ways to propagate the
message of the Master to the masses, have shied away from these hastily
written, disturbingly unrefined and amorphous notes.

[^notebooks]: 
  {-} [*The Ethnological Notebooks of Karl
  Marx*](https://www.marxists.org/archive/marx/works/1881/ethnographical-notebooks/index.htm),
  edited by Lawrence Krader (Assen, The Netherlands: Van Gorcum, 1974).

The neglect of the notebooks for nearly a century is even less surprising when
one realizes the degree to which they challenge what has passed for Marxism all
these years. In the lamentable excuse for a "socialist" press in the
English-speaking world, this last great work from Marx's pen has been largely
ignored. Academic response, by anthropologists and others, has been practically
nonexistent, and has never gone beyond Lawrence Krader's lame assertion, at the
end of his informative 85-page Introduction, that the *Notebooks*' chief
interest is that they indicate "the transition of Marx from the restriction of
the abstract generic human being to the empirical study of particular peoples."
It would seem that even America's most radical anthropologists have failed to
come to grips with these troubling texts. The *Notebooks* are cited only once
and in passing in Eleanor Leacock's *Myths of Male Dominance: Collected
Articles on Women Cross-Culturally*. And Stanley Diamond, who Krader thanks for
reading his Introduction, makes no reference to them at all in his admirable
study, *In Search of The Primitive: A Critique of Civilization*.

The most insightful commentary on these *Notebooks* has naturally come from
writers far outside the mainstream---"Marxist" as well as academic. Historian,
antiwar activist and Blake scholar E. P. Thompson, in his splendid polemic,
*The Poverty of Theory and Other Essays*, was among the first to point out that
"Marx, in his increasing preoccupation in his last years with anthropology, was
resuming the projects of his Paris youth." Raya Dunayevskaya, in her *Rosa
Luxemburg, Women's Liberation and Marx's Philosophy of Revolution*, is more
explicit in her estimate of these "epoch-making *Notebooks* which rounded out
Marx's life work": these "profound writings that ... summed up his life's work
and created new openings," and which therefore have "created a new
vantage-point from which to view Marx's oeuvre as a totality." Dunayevskaya, a
lifelong revolutionist and a pioneer in the revival of interest in the Hegelian
roots of Marxism, argued further that 

> these *Notebooks* reveal, at one and the same time, the actual ground that
> led to the first projection of the possibility of revolution coming first in
> the underdeveloped countries like Russia; a reconnection and deepening of
> what was projected in the *Grundrisse* on the Asiatic mode of production;
> *and* a return to that most fundamental relationship of Man/Woman which had
> first been projected in the 1844 essays.

The suggestion that the *Ethnological Notebooks* signify Marx's return to the
"projects of his Paris youth" might turn out to entail more far-reaching
implications than anyone has yet realized. Marx's *Economic and Philosophical
Manuscripts of 1844* are unquestionably the brightest star of that heroic early
period, but they should be seen as part of a whole constellation of
interrelated activities and aspirations.

One of the first things that strikes us about Marx's Paris youth is that this
period precedes the great splits that later rent the revolutionary workers'
movement into so many warring factions. Marxists of all persuasions even though
bitterly hostile to each other, have nonetheless tended to agree that these
splits enhanced the proletariat's organizational efficacy and theoretical
clarity, and therefore should be viewed as positive gains for the movement as a
whole. But isn't it just possible that, in at least some of these splits,
something not necessarily horrible or worthless was *lost* at the same time? In
any event, in 1844--45 we find Marx in a veritable *euphoria* of self-critical
exploration and discovery: sorting out influences, puzzling over a staggering
range of problems, and "thinking out loud" in numerous manuscripts never
published in his lifetime. In his Paris youth, and for several years
thereafter, Karl Marx was no Marxist.

Early in 1845, for example, he and his young friend Engels were
enthusiastically preparing an unfortunately-never-realized "Library of the Best
Foreign Socialist Authors," which was to have included works by Théophile
Leclerc and other *enragés*, as well as by Babeuf and Buonarroti, William
Godwin, Fourier, Cabet and Proudhon---that is, representative figures from the
entire spectrum of revolutionary thought outside all sectarianism. They were
especially taken with the prodigious work of the most inspired and daring of
the utopians, Charles Fourier, who had died in 1837, and for whom they would
retain a profound admiration all their lives. Proudhon on the other hand,
influenced them not only through his books, but---at least in Marx's
case---personally as well, for he was a good friend in those days, with whom
Marx later recalled having had "prolonged discussions" which often lasted "far
into the night."

It is too easily forgotten today that in 1844 Proudhon already enjoyed an
international reputation; his *What Is Property?* (1840) had created an
enormous scandal, and no writer was more hated by the French bourgeoisie.
Marx, an unknown youth of 26, still had much to learn from the ebullient
journeyman printer who would come to be renowned as the "Father of Anarchism":
In his first book, *The Holy Family* (1845), Marx hailed *What is Property?* as
"the first resolute, ruthless, and at the same time scientific investigation
... of the basis of political economy, private property ... an advance which
revolutionizes political economy and for the first time makes a real science of
political economy possible."

In 1844 we find Engels writing sympathetically of American Shaker communities,
which he argued, proved that "communism ... is not only possible but has
actually already been realized." The same year he wrote a letter to Marx
praising Max Stirner's new work, *The Ego and Its Own*, urging that Stirner's
very egoism "can be built upon even as we invert it" and that "what is true in
his principles we have to accept"; an article suggesting that the popularity of
the German translation of Eugene Sue's quasi-Gothic romance *The Mysteries of
Paris* proved that Germany was ripe for communist agitation; and a letter to
the editor defending an "author of several Communist books," Abbe Constant,
who, under the name he later adopted---Éliphas Lévi---would become the most
renowned of French occultists.

Constant was a close friend of pioneer socialist-feminist Flora Tristan, whose
*L'Union ouvrière* (Workers' Union, 1842) was the first work to urge working
men and women to form an international union to achieve their emancipation. One
of the most fascinating personalities in early French socialism, Tristan was
given a place of honor in *The Holy Family*, zealously defended by Marx from
the stupid, sexist gibes of the various counter-revolutionary "Critical
Critics" denounced throughout the book.

That Constant became a practicing occultist, and that he and Tristan were for
several years closely associated with the mystical socialist and phrenologist
Simon Ganneau, "messiah" of a revolutionary cult devoted to the worship of an
androgynous divinity, reminds us that Paris in the 1830s and '40s was the scene
of a remarkable reawakening of interest in things occult, and that the
*milieux* of occultists and revolutionists were by no means separated by a
Chinese wall. A new interest in alchemy was especially evident, and important
works on the subject date from that period, notably the elusive Cyliani's
*Hermès dévoilé* (1832)---reprinted in 1915, this became a key source for the
Fulcanelli circle, which in turn inspired our own century's hermetic
revival---and François Cambriel's *Cours de Philosophie hermétique, Ou
d'Alchimie en dix-neuf leçons* (1843).

To what extent Marx and/or Engels encountered occultists or their literature is
not known, and is certainly not a question that has interested any of their
biographers. It cannot be said that the passing references to alchemy and the
Philosophers' Stone in their writings indicate any familiarity with original
hermetic sources. We do know, however, that they shared Hegel's high esteem for
the sixteenth century German mystic and heretic Jacob Boehme, saluted by Marx
in the *Rheinische Zeitung* in 1842 as "a great philosopher." Four years
earlier Engels had made a special study of Boehme, finding him "a dark but deep
soul," "very original" and "rich in poetic ideas." Boehme is cited in *The Holy
Family* and in several other writings of Marx and Engels over the years.

One of the things that may have attracted them to Boehme is the fact that he
was very much a dialectical thinker. Dialectic abounds in the work of many
mystical authors, not least in treatises on magic, alchemy and other "secret
sciences" and it should astonish no one to discover that rebellious young
students of Hegel had made surreptitious forays onto this uncharted terrain in
their quest for knowledge. This was certainly the case with one of Marx's close
friends, a fellow Young Hegelian, Mikhail Bakunin, who often joined him for
those all-night discussions at Proudhon's. As a young man the future author of
*God and the State* is known to have studied the works of the French mystic,
Louis Claude de Saint-Martin, "The Unknown Philosopher" and "Lover of Secret
things" as well as of the eccentric German romantic philosopher, Franz von
Baader, author of a study of the mysterious eighteenth-century
Portuguese-Jewish mage, Martinez de Pasqually, who is thought by some to have
had a part in the formation of Haitian voodoo (he spent his last years on the
island and died in Port-au-Prince in 1774), and whose *Traité de la
réintegration* is one of the most influential occult writings of the last two
centuries.

Mention of von Baader, whose romantic philosophy combined an odd Catholic
mysticism and equally odd elements of a kind of magic-inspired utopianism that
was all his own---interestingly, he was the first writer in German to use the
word "proletariat"---highlights the fact that Boehme, Paracelsus, Meister
Eckhart. Swedenborg, Saint-Martin and all manner of wayward and mystical
thinkers contributed mightily to the centuries-old ferment that finally
produced Romanticism, and that Romanticism in turn, especially in its most
extreme and heterodox forms, left its indelible mark on the Left
Hegelian/Feuerbachian milieu. Wasn't it under the sign of *poetry*, after all
that Marx came to recognize himself as an enemy of the bourgeois order?
Everyone knows the famous "three components" of Marxism: German philosophy,
English economics and French socialism. But what about the poets of the world:
Aeschylus and Homer and Cervantes, Goethe and Shelley? To miss this fourth
component is to miss a lot of Marx (and indeed, a lot of *life*). A whole
critique of post-Marx Marxism could be based on this calamitous "oversight."
1844, one does well to remember, was also a year in which Marx was especially
close to Heinrich Heine. Marx himself wrote numerous poems of romantic frenzy
(two were published in 1841 under the title "*Wild Songs*") and even tried his
hand at a play and a bizarre satirical romance *Scorpion and Felix*. By 1844 he
had renounced literary pursuits as such, but no philosopher, no political
writer or activist and certainly no economist has ever used metaphor with such
exuberance and flair as the author of *A Contribution to the Critique of
Political Economy* used throughout his life. To the last. Marx---and to a great
extent this is also true of Engels---remained a fervent adept of "poetry's
magic fullness" (to quote one of his early translations of Ovid's elegies).
These ardent youths never ceased to pursue philosophy on their road to
revolution, but it was poetry that, as often as not, inspired their daring and
confirmed their advances.

---

[That Marx, toward the end of his life]{.newthought}, was returning to projects
that had been dear to his heart in the days of his original and bold grappling
with "naturalist anthropology" as a theory of communist revolution, the days in
which he was most deeply preoccupied with the philosophical and practical
legacy of Hegel and Fourier, the days of his friendship with Proudhon and
Bakunin and Heine, is resonant with meanings for today---all the more so, since
here, too, at the end as at the beginning a crucial motivating impulse seems to
have been provided by poetry.

In 1880 the publication of James Thomson's *City of Dreadful Night, and Other
Poems*---the title piece of which is often called the most pessimistic poem in
the English language---made a powerful impression on the author of *Capital*.
Especially enthusiastic about Thomson's "Attempts at Translations of Heine,"
Marx wrote a warm letter to the poet, urging that the poems were "no
translations, but a reproduction of the original, such as Heine himself, if
master of the English language, would have given." Although Marx's biographers
have maintained an embarrassed silence on the subject, it is really not so
difficult to discern how Thomson---this opium-addicted poet of haunting black
lyricism, who was not only one of the most aggressive anti-religious agitators
in English but also the translator of Leopardi and among the first to write
intelligently about Blake---could have stimulated a revival of the dreams and
desires of Marx's own most Promethean days. And then, just think of it: while
his brain is still reeling with visions inspired by a true poet, he plunges
into the richest, most provocative work of the most brilliant anthropological
thinker of his time. Such chances are the very stuff that revelations are made
of!

[^morgan] It was not mere "anthropology," however, that Marx found so appealing in Lewis
Henry Morgan's *Ancient Society*, but rather, as he hints in his notes and as
Engels spelled out in his *Origin of the Family, Private Property and the
State* (1884), the merciless critique and condemnation of capitalist
civilization that so well complements that of Charles Fourier.

[^morgan]:
  {-} ![Lewis Henry
  Morgan](https://upload.wikimedia.org/wikipedia/commons/0/0c/Lewis_henry_morgan.jpg)

And yet these *Ethnological Notebooks* are much more than a
compilation of new data confirming already existing criticism. It must be said,
in this regard, that *The Origin of the Family*, which Engels says he wrote as
"the fulfillment of a bequest"---Marx having died before he was able to prepare
his own presentation of Morgan's researches---is, as Engels himself readily
admitted, "but a meager substitute" for the work Marx's notes suggest. Several
generations of Marxists have mistaken *The Origin of the Family* for the
definitive word on the subject, but in fact it reflects Engels' reading of
Morgan (and other authors) far more than it reflects Marx's notes. Engels'
sweeping notion of "the world-historic defeat of the female sex," for example,
was borrowed from the writings of J. J. Bachofen, and is not well supported by
Marx's notes, while several important comments that Marx *did* make were not
included in Engels' little book.

Clearly intending *The Origin of the Family* to be nothing more than a popular
socialist digest of the major themes of *Ancient Society*---Morgan's famous
systems of consanguinity, his extensive data on "communism in living," the
evolution of property and the State---Engels emphasized Morgan's broad
agreement with Marx and ignored everything in Morgan and in Marx that lay
outside this modest plan. That Engels did not write the book that Marx might
have written is not really such shocking news, and any blame for possible
damage done would seem to rest not with Engels but with all those who, since
1884, devoutly *assumed* that Engels' book said all that Marx had to say and
therefore all that had to be said. Of course, had Marx's followers taken to
heart his own favorite watchword, *De omnibus dubitandum* ("doubt everything"),
the history of Marxism would have been rather different and probably much
happier. And as the blues singer sang, "If a frog had wings..."

The *Notebooks* include excerpts from, and Marx's commentary on, other
ethnological writers besides Morgan, but the section on Morgan is the most
substantial by far, and of the greatest interest. Reading this curious dialogue
one can almost see Marx's mind at work---sharpening, extending, challenging and
now and then correcting Morgan's interpretations, bringing out dialectical
moments latent in *Ancient Society* but not always sufficiently developed, and
sometimes wholly undeveloped, by Morgan himself. Marx also seemed to enjoy
relating Morgan's empirical data to the original sources of his (Marx's) own
critique, notably Fourier and (though his name does not figure in these notes)
Hegel, generally with the purpose of clarifying some vital current problem. As
Marx had said of an earlier unfinished work, the *Grundrisse* (1857--58), the
*Ethnological Notebooks* contain "some nice developments."

Some of the most interesting passages by Marx that did not find their way into
Engels' book have to do with the transition from "archaic" to "civilized"
society, a key problem for Marx in his last years. Questioning Morgan's
contention that "personal government" prevailed throughout primitive societies,
Marx argued that long before the dissolution of the gens (clan), chiefs were
"elected" only in theory, the office having become a transmissible one,
controlled by a property-owning elite that had begun to emerge within the gens
itself. Here Marx was pursuing a critical inquiry into the origins of the
distinction between public and private spheres (and, by extension, between
"official" and "unofficial" social reality and ideological fiction) that he had
begun in his critique of Hegel's philosophy of law in 1843. The close
correlation Marx found between the development of property and the state, on
the one hand, and religion, their chief ideological disguise, on the
other---which led to his acute observation that religion grew as the gentile
commonality shrank---also relates to his early critique of the
*Rechtphilosophie*, in the famous introduction to which Marx's attack on
religion attained an impassioned lucidity worthy of the greatest poets.

The poetic spirit, in fact, makes its presence felt more than once in these
*Notebooks*. Auspiciously, in this compendium of ethnological evidence, Marx
duly noted Morgan's insistence on the historical importance of "*imagination*,
that great faculty so largely contributing to the elevation of mankind." From
cover to cover of these *Notebooks* we see how Marx's encounter with "primitive
cultures" stimulated his own imagination, and we begin to realize that there is
much more here than Engels divulged.

On page after page Marx highlights passages wildly remote from what are usually
regarded as the "standard themes" of his work. Thus we find him invoking the
bell-shaped houses of the coastal tribes of Venezuela; the manufacture of
Iroquois belts "using fine twine made of filaments of elm and basswood bark,"
"the Peruvian legend of Manco Capac and Mama Oello, children of the sun";
burial customs of the Tuscarora; the Shawnee belief in metempsychosis;
"unwritten literature of myths, legends and traditions"; the "incipient
sciences" of the village Indians of the Southwest; the *Popul Vuh*, sacred book
of the ancient Quiché Maya; the use of porcupine quills in ornamentation;
Indian games and "dancing \[as a\] form of worship."

Carefully, and for one tribe after another, Marx lists each of the animals from
which the various clans claim descent. No work of his is so full of such words
as wolf, grizzly bear, opossum and turtle (in the pages on Australian
aborigines we find emu, kangaroo and bandicoot). Again and again he copies
words and names from tribal languages. Intrigued by the manner in which
individual (personal) names indicate the gen, he notes these Sauk names from
the Eagle gens: "Ka-po-na ('Eagle drawing his nest'); Ja-ka-kwa-pe ('Eagle
sitting with his head up'); Pe-a-ta-na-ka-hok ('Eagle flying over a limb')."
Repeatedly he attends to details so unusual that one cannot help wondering what
he was thinking as he wrote them in his notebook. Consider, for example, his
word-for-word quotation from Morgan telling of a kind of "grace" said before an
Indian tribal feast: "It was a prolonged exclamation by a single person on a
high shrill note, falling down in cadences into stillness, followed by *a
response in chorus by the people*." After the meal, he adds, "The evenings
\[are\] devoted to dance."

Especially voluminous are Marx's notes on the Iroquois, the confederation of
tribes with which Morgan was personally most familiar (in 1846 he was in fact
"adopted" by one of its constituent tribes, the Seneca, as a warrior of the
Hawk clan), and on which he had written a classic monograph. Clearly Marx
shared Morgan's passional attraction for the "League of the Ho-de-no-sau-nee"
among whom "the state did not exist," and "Liberty, Equality and Fraternity,
though never formulated, were cardinal principles," and whose sachems,
moreover, had "none of the marks of a priesthood." One of his notes includes
Morgan's description of the formation of the Iroquois Confederation as "a
masterpiece of Indian wisdom," and it doubtless fascinated him to learn that,
as far in advance of the revolution as 1755, the Iroquois had recommended to
the "forefathers \[of the\] Americans, *a union of the colonies similar to
their own*."

Many passages of these *Notebooks* reflect Marx's interest in Iroquois
democracy as expressed in the Council of the Gens, that "democratic assembly
where *every adult male and female member* had a voice upon all questions
brought before it," and he made special note of details regarding the active
participation of women in tribal affairs. The relation of man to woman---a
topic of Marx's 1844 manuscripts---is also one of the recurring themes of his
ethnological inquiries. Thus he quotes a letter sent to Morgan by a missionary
among the Seneca: 

> *The women were the great power among the clans, as everywhere else*. They
> did not hesitate, when occasion required, 'to knock off the horns,' as it was
> technically called, from the head of a chief, and send him back to the ranks
> of the warriors. *The original nomination of the chief also always rested
> with them*.

And a few pages later he highlights Morgan's contention that the "present
*monogamian family* ... must ... change as society changes ... It is the
creature of a social system ... capable of still further improvement *until the
equality of the sexes is attained*." He similarly emphasizes Morgan's
conclusion, regarding monogamy, that "it is impossible to predict the nature of
its successor."

In this area as elsewhere Marx discerned germs of social stratification within
the gentile organization, again in terms of the separation of "public" and
"private" spheres, which he saw in turn as the reflection of the gradual
emergence of a propertied and privileged tribal caste. After copying Morgan's
observation that, in the Council of Chiefs, women were free to express their
wishes and opinions "*through an orator of their own choosing*" he added, with
emphasis, that the "*Decision* \[was\] made by the (all-male) Council." Marx
was nonetheless unmistakably impressed by the fact that, among the Iroquois,
women enjoyed a freedom and a degree of social involvement far beyond that of
the women (or men!) of any civilized nation. The egalitarian tendency of all
gentile societies is one of the qualities of these societies that most
interested Marx, and his alertness to deviations from it did not lead him to
reject Morgan's basic hypothesis in this regard. Indeed, where Morgan, in his
chapter on "The Monogamian Family," deplored the treatment of women in ancient
Greece as an anomalous and enigmatic departure from the egalitarian norm, Marx
commented (perhaps here reflecting the influence of Bachofen): "But the
relationship between the goddesses on Olympus reveals memories of women's
higher position?"

Marx's passages from Morgan's chapters on the Iroquois are proportionally much
longer than his other excerpts from *Ancient Society*, and in fact make up one
of the largest sections of the *Notebooks*. It was not only Iroquois social
organization, however, that appealed to him, but rather *a whole way of life*
sharply counterposed, all along the line, to modern industrial civilization.
His overall admiration for North American Indian societies generally, and for
the Iroquois in particular, is made clear throughout the text, perhaps most
strongly in his highlighting of Morgan's reference to their characteristic
"sense of independence" and "personal dignity," qualities both men appreciated
but found greatly diminished as humankind's "property career" advanced.
Whatever reservations Marx may have had regarding the universal applicability
of the Iroquois "model" in the analysis of gentile societies, the painstaking
care with which he copied out Morgan's often meticulous descriptions of the
various aspects of their culture shows how powerfully these people impressed
him. Whole pages of the *Notebooks* recount, in marvelous detail, Iroquois
Council procedures and ceremonies:

> at a signal the sachems arose and marched 3 times around the Burning Circle,
> going as before by the North ... Master of the ceremonies again rising to his
> feet, filled and lighted the pipe of peace from his own fire; drew 3 whiffs,
> the first toward the Zenith (which meant thanks to the Great Spirit ...); the
> second toward the ground (means thanks to his Mother, the Earth, for the
> various productions which had ministered to his sustenance); third toward the
> Sun (means thanks for his never-failing light, ever shining upon all). Then
> he passed the pipe to the first upon his right toward the North ...

This passage goes on in the same vein for some thirty lines, but I think this
brief excerpt suffices to show that the *Ethnological Notebooks* are unlike
anything else in the Marxian canon.

---

[The record of Marx's vision quest]{.newthought} through Morgan's *Ancient
Society* offers us a unique and amazing close-up of the final phase of what
Raya Dunayevskaya has called Marx's "never-ending search for new paths to
revolution." The young Marx of the *Economic and Philosophical Manuscripts of
1844* summed up revolution as "the supersession of private property." His
starting-point was the critique of *alienated labor* which "alienates nature
from man, man from himself ... \[and man\] from the species"---that is, labor
dominated by the system of *private property*, by *capital*, the "*inhuman
power*" that "rules over everything," spreading its "infinite degradation" over
the fundamental relation of man to woman and reducing all human beings to
commodities. Thus the "supersession of private property" meant for Marx not
only the "emancipation of the workers" (which of course involves "the
emancipation of humanity as a whole"), but also "the emancipation of all the
human qualities and senses" (the senses themselves having become directly, as
he expressed it with characteristic humor, "theoreticians in practice"). This
"*positive* abolition of private property, of human *self-alienation*," is
also, at the same time, "the real *appropriation* of *human* nature"---in other
words, *communism*,

> the definitive resolution of the antagonism between man and nature, and
> between man and man. It is the true solution of the conflict between
> existence and essence, between objectification and self-affirmation, between
> freedom and necessity, between individual and species. It is the solution of
> the riddle of history and knows itself to be this solution.

To such ways of seeing the old Marx seems to have returned as, in his mind's
eye, he took his three whiffs on the pipe of peace around the Iroquois council
fire. But it was no self-indulgent nostalgia that led him to trace the perilous
path of his youthful dreams and beyond, to the dawn of human society. A
revolutionist to the end, Marx in 1880 no less than in 1844 envisioned a
radically new society founded on a total transformation in human relationships,
and sought new ways to help bring this new society into being.

*Ancient Society*, and especially its detailed account of the Iroquois, for the
first time gave Marx insights into the concrete *possibilities* of a free
society *as it had actually existed in history.* Morgan's conception of social
and cultural evolution enabled him to pursue the problems he had taken up
philosophically in 1844 in a new way, from a different angle, and with new
revolutionary implications. Marx's references, in these notes and elsewhere, to
terms and phrases recognizable as Morgan's, point toward his general acceptance
of Morgan's outline of the evolution of human society. Several times in the
non-Morgan sections of the *Notebooks*, for example, he reproaches other
writers for their ignorance of the character of the gens, or of the "Upper
Status of Barbarism." In drafts of a letter written shortly after reading
Morgan, he specified that "Primitive communities ... form a series of social
groups which, differing in both type and age, mark successive phases of
evolution." But this does not mean that Marx adopted, in all its details, the
so-called "unilinear" evolutionary plan usually attributed to Morgan---a plan
which, after its uncritical endorsement by Engels in *The Origin of the
Family*, has remained ever since a fixture of "Marxist" orthodoxy. Evidence
scattered throughout the *Notebooks* suggests, rather, that Marx had grown
markedly skeptical of fixed categories in attempts at historical
reconstruction, and that he continued to affirm the multilinear character of
human social development that he had advanced as far back as the *Grundrisse*
in the 1850s.

Indeed, it is amusing, in view of the widespread misapprehension of Morgan as
*nothing but* a monomaniacal unilinearist, that Marx's notes highlight various
departures from unilinearity in Morgan's own work. Morgan himself, in fact,
more than once acknowledged the "provisional" character of his system, and
especially of the "necessarily arbitrary" character of the boundary lines
between the developmental stages he proposed; he nonetheless regarded his
schemata as "convenient and useful" for comprehending such a large mass of
data, and in any case specifically allowed for (and took note of) exceptions.

However, if our reading of Marx's notes is right, he found things in *Ancient
Society* infinitely more valuable to him than arguments for or against any mere
classificatory system. The book's sheer immensity of new information---new for
Marx and for the entire scientific world---demonstrated conclusively the true
complexity of "primitive" societies as well as their grandeur, their essential
superiority, in real human terms, to the degraded civilization founded on the
fetishism of commodities. In a note written just after his conspectus of Morgan
we find Marx arguing that "primitive communities had *incomparably greater
vitality* than the Semitic, Greek, Roman and *a fortiori* the modern capitalist
societies." Thus Marx had come to realize that, measured according to the
"wealth of subjective human sensuality," as he had expressed it in the 1844
manuscripts, Iroquois society stood *much higher* than any of the societies
"poisoned by the pestilential breath of civilization." Even more important,
Morgan's lively account of the Iroquois gave him a vivid awareness of the
*actuality of indigenous peoples*, and perhaps even a glimpse of the
then-undreamed of possibility that such peoples could make *their own*
contributions to the global struggle for human emancipation.

---

[Hard hit as they had been]{.newthought} by the European capitalist invasion
and U.S. capitalism's westward expansion, the Iroquois and other North American
tribal cultures could not in the 1880s and cannot now, a hundred years later,
be consigned to the museums of antiquity. When Marx was reading *Ancient
Society* the "Indian wars" were still very much a current topic in these United
States, and if by that time the military phase of this genocidal campaign was
confined to the west, far from Iroquois territory, still the Iroquois, and
every surviving tribal society, were engaged (as they are engaged today to one
degree or another) in a continuous struggle against the system of private
property and the State.

In a multitude of variants, the same basic conditions prevailed in Asia,
Africa, parts of Eastern Europe, Russia, Canada, Australia, South America, the
West Indies, Polynesia---wherever indigenous peoples had not wholly succumbed
to the tyranny of capitalist development. After reading Morgan's portrayal of
primitive communism at the height of its glory, Marx saw all this in a new
light. In the last couple of years of his life, to a far greater degree than
ever before, he focused his attention on people of color: the colonized,
peasants and "primitives."

That he was not reading Morgan exclusively or even primarily for historical
purposes, but rather as part of his ongoing exploration of the processes of
revolutionary social change, is suggested by numerous allusions in the
*Notebooks* to contemporary social/political affairs. In the *Notebooks*, as
Raya Dunayevskaya has argued, "Marx's hostility to capitalism's colonialism was
intensifying ... \[He\] returns to probe the origin of humanity, *not* for
purposes of discovering new origins, but for perceiving new revolutionary
forces, their reason, or as Marx called it, in emphasizing a sentence of
Morgan, 'powers of the mind.'"

The vigorous attacks on racism and religion that recur throughout the
*Notebooks*, especially in the often lengthy and sometimes splendidly
vituperative notes on Maine and Lubbock, leave no doubt in this regard.

Again and again when these smirking apologists for imperialism direct their
condescending ridicule at the "superstitious" beliefs and practices of
Australian aborigines or other native peoples, Marx turns it back like a
boomerang on the "civilized canaille." He accepted---at least, he did not
contradict---Lubbock's hypothesis that the earliest human societies were
atheist, but had only scorn for Lubbock's specious reasoning: that the savage
mind was not developed enough to recognize the "truths" of religion! No, Marx's
notes suggest, our "primitive" ancestors were atheists because the belief in
gods and other priestly abominations entered the world only with the beginnings
of class society. Relentlessly, in these notes, he follows the development of
religion as an integral part of the repressive apparatus through its various
permutations linked to the formation of caste, slavery, patriarchal monogamy
and monarchy. The "poor religious element," he remarks, becomes the main
preoccupation of the gens precisely to the degree that real cooperation and
common property decline, so that eventually, "only the smell of incense and
holy water remains." The author of the *Ethnological Notebooks* made no secret
of the fact that he was solidly on the side of the atheistic savages.

After poring over *Ancient Society* at the end of 1880 and the first weeks of
'81, a large share of Marx's reading focused on primitive societies and
"backward" countries. Apart from the works of John Budd Phear, Henry Sumner
Maine and John Lubbock that he excerpted and commented on in the *Ethnological
Notebooks* he read books on India, China and Java, and several on Egypt (two
and a half months before his death, in a letter to his daughter Eleanor, Marx
denounced the "shameless Christian-hypocritical conquest" of Egypt). After he
returned from a brief visit to Algiers in the spring of '82, his son-in-law
Paul Lafargue wrote that "Marx has come back with his head full of Africa and
Arabs." When he received a query from the Russian radical Vera Zasulich, asking
whether the Russian rural communes could become the basis for a new collective
society or whether her homeland would have to pass through a capitalist stage,
Marx intensified his already deep study of Russian social and economic
history.[^zasulich] His remarkable reply to Zasulich offers a measure of Marx's
creative audacity in his last years, and demonstrates too, that his reading of
Morgan involved not only a new way of looking at pre-capitalist societies, but
also a new way of looking at the latest practical problems facing the
revolutionary movement.  Zasulich's letter to Marx had more than a hint of
urgency about it, for, as she explained,

[^zasulich]:
  {-} [The Zasulich--Marx
  correspondence.](https://www.marxists.org/archive/marx/works/1881/zasulich/)

> Nowadays, we often hear it said that the rural commune is an archaic form
> condemned to perish by history, scientific socialism and, in short,
> everything above debate. Those who preach such a view call themselves your
> disciples ... their strongest argument is often: "Marx said so." But how do
> you derive that from *Capital*?" others object. "He does not discuss the
> agrarian question, and says nothing about Russia." "He would have said as
> much if he had discussed our country," your disciples retort ...

Just how seriously Marx pondered the question may be inferred from the fact
that he wrote no less than four drafts of a reply in addition to the
comparatively brief letter he actually sent---a grand total of some twenty-five
book pages. His reply was a stunning blow to the self-assured, dogmatic
smugness of the Russian "Marxists" who not only refused to publish the letter
but pretended that it did not exist (it was published for the first time in
1924).

Stressing that the "historical inevitability" of capitalist development as
articulated in *Capital* was "*expressly* restricted to the *countries of
Western Europe*," he concluded that

> The analysis in Capital therefore provides no reasons---either for or
> against---the vitality of the Russian Commune. But the special study I have
> made of it, including a search for original source-material, has convinced me
> that the commune is the fulcrum for social regeneration in Russia.

The Preface to the second Russian edition of the *Communist Manifesto* (1882),
co-signed by Engels, closed with a somewhat qualified restatement of this new
orientation:

> Can the Russian obshchina \[peasant commune\], a form, albeit highly eroded,
> of the primitive communal ownership of the Land, pass directly into the
> higher, communist form of communal ownership? ... Today there is only one
> possible answer. If the Russian revolution becomes the signal for proletarian
> revolution in the West, so that the two complement each other, then Russia's
> peasant communal land-ownership may serve as the point departure for a
> communist development.

The bold suggestion that revolution in an underdeveloped country might precede
and precipitate revolution in the industrialized West did not pop up out of
nowhere---every idea has its prehistory---but few will deny that it
contradicts, uproariously, the overwhelming bulk of Marx's anterior work. It is
in fact, a flagrantly "anti-Marxist" heresy, as Marx's Russian disciples surely
were aware. Just six years earlier, in 1875, a Russian Jacobin, Petr Tkachev,
brought down upon himself a good dose of Engels' ridicule---evidently with
Marx's full approval---for having had the temerity to propose some such
nonsense about skipping historically ordained stages, and even the appalling
fantasy that peasant-riddled Russia could reach the revolutionary starting line
before the sophisticated proletariat of the West. Such "pure hot air," Engels
felt obliged to counsel the poor Russian "schoolboy," proved only that Tkachev
had yet "to learn the ABC of Socialism."

Marx's growing preoccupation with revolutionary prospects in Russia during the
last decade of his life is a subject scrutinized from many angles and with
marvelous insight in Teodor Shanin's *Late Marx and the Russian Road*, a book
of impeccable scholarship that is also a major contribution to the
clarification of revolutionary perspectives today. As Shanin and his
collaborators have shown, Marx was hostile to Russian Populism in the 1860s,
but began to change his mind early in the next decade when he taught himself
Russian and started reading Populist literature, including works by the
movement's major theorist, N. G. Chernyshevsky, for whom he quickly developed
the deepest admiration. By 1880 Marx was a wholehearted supporter of the
revolutionary Populist *Narodnaya Volya* (People's Will), even defending its
terrorist activities (the group attempted to assassinate the Czar that year,
and succeeded the next), while remaining highly critical of the "boring
doctrines" of Plekhanov and other would-be Russian "Marxists" whom he derided
as "defenders of capitalism." Throughout this period Marx read avidly in the
field of Russian history and economics; a list he made of his Russian books in
August 1881 included nearly 200 titles.

The iconoclastic reply to Zasulich then, was conditioned by many factors,
including the formation of a new Russian revolutionary movement, personal
meetings with Populists and others from Russia, and Marx's wide reading of
scholarly and popular literature, as well as radical and bourgeois newspapers.

Several provocative coincidences relate *Ancient Society* to this major shift
in Marx's thought. First, Marx originally borrowed a copy of the book from one
of his Russian visitors, Maxim Kovalevsky, who had brought it back from a trip
to the U.S. Whether this was the copy Marx excerpted is not known; Engels did
not find the book on Marx's shelves after his death. But Morgan's work aroused
interest among other Russian revolutionary *émigrés* as well, for we know that
Marx's longtime friend Petr Lavrov, a First-Internationalist and one of the
most important Populists, also owned a copy, which he had purchased at a London
bookshop. These are the only two copies of the book known to have existed in
Marx's immediate milieu during his lifetime.'

Second, Marx's Morgan excerpts include interpolated comments of his own on the
Russian commune. The *Notebooks* also touch on other themes---most notably the
skipping of stages by means of technological diffusion between peoples at
different stages of development---that recur in the drafts of the letter to
Zasulich.

Third, and more strikingly, Zasulich's letter to Marx reached him just as he
was in the midst of, or had just completed, making these annotated excerpts
from Morgan's work.

Fourth, and most important of all, Marx cited and even quoted---or rather
paraphrased---Morgan in a highly significant passage in one of the drafts of
his reply to Zasulich:

> the rural commune \[in Russia\] finds \[capitalism in the West\] in a state
> of crisis that will end only when the social system is eliminated through the
> return of modern societies to the "archaic" type of communal property. In the
> words of an American writer who, supported in his work by the Washington
> government, is not at all to be suspected of revolutionary tendencies \[here
> Marx refers to the fact that Morgan's *Systems of Consanguinity and Affinity*
> was published by the Smithsonian Institution\] "the new system" to which
> modern society is tending "will be a revival, in a superior form, of an
> archaic social type." We should not then, be too frightened by the word
> archaic.

Scattered through the drafts of his letter to Zasulich, moreover, are a half
dozen other unmistakable allusions to Morgan's researches.

Thus we have ascertained that Zasulich's letter arrived at a time when *Ancient
Society* was very much on Marx's mind. Taken together, the foregoing
"coincidences" strongly urge upon us the conclusion that Marx's reading of
Morgan was an active factor in the qualitative leap in his thought on
revolution in underdeveloped countries.

---

[If America's "radical intelligentsia" were something more]{.newthought} than
an academically domesticated sub-subculture of hyper-timid and
ultra-respectable seekers of safe-at-all-cost careers, Marx's *Ethnological
Notebooks* might have spearheaded, among other things, a revival of interest in
Lewis Henry Morgan.  But no, the *Notebooks* have been conveniently ignored
and, notwithstanding a few faint glimmers of change in the 1960s, the
near-universal contempt for the author of *Ancient Society* remains in full
force today.

Even so perceptive and sensitive a critic as Raya Dunayevskaya did not entirely
avoid the unfortunate Morgan-bashing that has been a compulsory ritual of
American anthropology, and of U.S. intellectual life generally, since the First
World War. In her case, of course, she was responding to rather different
rituals on the opposite side of the ideological fence: to what one could call
the pseudo-Marxists' pseudo-respect for Morgan. In truth, however, the
traditional rhetorical esteem for Morgan on the part of Stalinists and social
democrats is only another form of contempt, for with few exceptions it was not
founded on a scrupulous reading of Morgan but an unscrupulous reading of
Engels.

Caught in the welter of a politically motivated and therefore all the more
highly emotional "debate" between equally careless would-be friends and
automatic enemies, Morgan's writings have been practically lost from sight for
decades.

Marx's enthusiasm for Morgan's work, discernible on every page of these
*Notebooks*, becomes obvious when one compares the Morgan notes to those on the
other ethnological writers whose books Marx excerpted: Sir John Phear, Sir
Henry Maine and Sir John Lubbock. The excerpts from Morgan are not only much
longer, half again as long as all the others combined---showing how deeply
interested Marx was in what Morgan had to say---but also are free of the
numerous and sometimes lengthy sarcastic asides sprinkled so liberally
throughout the other notes. Moreover, while Marx's disagreements with the
others are many and thoroughgoing, his differences with Morgan, as Krader
admits, are "chiefly over details." As a longtime "disciple of Hegel," Marx
disapproved by means of a parenthetical question mark and exclamation point of
an inexact use of the adjective "absolute." He further disputed Morgan's
interpretation of a passage from the *Iliad*, and another by Plutarch, neither
of them central to Morgan's argument. Such differences do not smack of the
insurmountable. Earlier I noted a few instances in which Marx's views diverged
from Morgan's on somewhat larger questions, but even these are as nothing
compared to his *complete* disagreement in *principle* with Maine and the
others. Indeed, at several points where Marx gave the "blockhead" and
"philistine" Maine and the "civilized ass" Lubbock a good pounding for their
shabby scholarship, their Christian hypocrisy, their bourgeois ethnocentrism
and racism, their inability to "free themselves of their own
conventionalities," he specifically cited Morgan as a decisive authority
against them.

Accepting Morgan's data and most of his interpretations as readily as he
rejected the inane ideological claptrap of England's royal ethnologists, with
their typically bourgeois mania for finding kings and capital in cultures where
such things do not exist. Marx was no doubt pleased to discover in *Ancient
Society* an arsenal of arguments in support of his own decidedly
anti-teleological revolutionary outlook. What matters, of course, is not so
much that Marx found Morgan to be, in many respects, a kindred spirit, or even
that he learned from him, but that the things he learned from Morgan were so
important to him.

However much his approach to Morgan may have differed from Engels's, Marx
certainly agreed with the latter's contention (in a letter to Karl Kautsky, 26
April 1884), that "Morgan makes it possible for us to look at things from
entirely new points of view." Reading *Ancient Society* appreciably deepened
his knowledge of many crucial questions, and qualitatively transformed his
thinking on other. The British socialist M. Hyndman, recalling conversations he
had with Marx during late 1880/early 1881, wrote in his memoirs that "when
Lewis Morgan proved to Marx's satisfaction that the gens and not the family was
the social unit of the old tribal system and ancient society generally, Marx at
once abandoned his previous opinions based upon Niebuhr and others, and
accepted Morgan's view." Anyone capable of making Karl Marx, at the age of 63,
abandon his previous opinions, is worthy of more than passing interest.

It was only after reading Morgan that anthropology, previously peripheral to
Marx's thought, became its vital center. His entire conception of historical
development, and particularly of pre-capitalist societies, now gained
immeasurably in depth and precision. Above all, his introduction to the
Iroquois and other tribal societies sharpened his sense of the living
*presence* of indigenous peoples in the world, and of their possible role in
future revolutions.

Reading Morgan, therefore, added far more than a few stray bits and pieces to
Marx's thought---it added *a whole new dimension*, one that has been suppressed
for more than a century and is only beginning to be developed today.

The careful re-evaluation of Morgan's work---for which Marx's notes on his
*magnum opus* provide such a stimulus---is surely a long-overdue project for
those who are struggling, with the clarity that comes only with despair, for
ways out of the manifold impasses to revolution in our time. Too often simply
reduced to a one-dimensional determinism and a bourgeois biologism, taken to
task *ad nauseam* for the alleged "rigidity" of his evolutionary system---which
he, however, held to be only "provisional"---Morgan is in fact a complex
figure: subtle, far-ranging, many-sided, non-academic, passionately drawn
toward poetry (his devotion to Shakespeare was as great as Marx's), and in many
ways more radical than even his relatively few sincere and knowledgeable
admirers have been willing to admit.

His sympathetic diary notes on the Paris Commune, made on his brief sojourn in
that city in June 1871, and his public defense of the Sioux during the
anti-Indian "Red Scare" following "Custer's Last Stand" in 1876---to cite only
two expressions of his dissident views on major issues of the day---show that
Morgan had little in common with the pedestrian image of the pious Presbyterian
and conservative burgher customarily used to characterize him. The strong
critical-utopian undercurrent in his work, especially evident in the many
remarkable parallels between his thought and Fourier's, but also in his
vehement anti-clericalism and his veneration for heretics such as Jan Hus, has
hardly been explored at all.

[^beaver] Let it not be forgotten, finally, that, apart from his epoch-making researches
in the field of anthropology, Morgan also left us a wonderful monograph on *The
American Beaver and His Works* (1868), a treatise pronounced "excellent" by
Charles Darwin, who cited it several times in *The Descent of Man*. In its last
chapter, Morgan bravely developed the notion of a "thinking principle" in
animals and came out for animal rights:

[^beaver]:
  {-} ![Plate from *The American Beaver and His
  Works*](https://upload.wikimedia.org/wikipedia/commons/thumb/5/52/The_American_beaver_and_his_works_%28Plate_I%29_BHL10622811.jpg/283px-The_American_beaver_and_his_works_%28Plate_I%29_BHL10622811.jpg)

> Is it to be the prerogative of man to uproot and destroy not only the masses
> of the animal kingdom numerically, but also the great body of the species? If
> the human family maintains its present hostile attitude toward \[animals\],
> and increases in numbers and in civilization at the present ratio, it is
> plain to be seen that many species of animals must be extirpated from the
> earth. An arrest of the progress of the human race can alone prevent the
> dismemberment and destruction of a large portion of the animal kingdom ...
> The present attitude of man toward the \[animals\] is not such as befits his
> Superior wisdom. We deny \[other species\] all rights, and ravage their ranks
> with wanton and unmerciful cruelty. The annual sacrifice of animal life to
> maintain human life is frightful ... when we claim that the bear was made for
> man food, we forget that man was just as much made to be food for the bear.

Morgan hoped that with the development of a friendlier, less prejudiced, more
intimate study of the other creatures of this planet, "our relations to them
will appear to us in a different, and in a better light."

---

[In the 1950s and '60s the revelations of "Early Marx"]{.newthought} gave the
lie alike to the oppressors of East and West. Early Marx, as millions
discovered for themselves, was the irreconcilable enemy not only of genocidal,
capitalist, "free enterprise" wage slavery, but also of institutionalized,
"official," bureaucratic state-capitalist "Marxism." Against *all* forms of
man's inhumanity to man: Marx's youthful revolutionary humanism helped inspire
a worldwide resurgence of radical thought and action that became known as the
"New Left" and gave the bosses and bureaucrats of all countries their biggest
scare since the Spanish Revolution of 1936. In an intellectual atmosphere
already bright with molotov cocktails tossed at Russian tanks by young workers
in Budapest in 1956, and at U.S. tanks by black youth in Chicago and dozens of
other U.S. cities ten years later; Marx's *Economic and Philosophical
Manuscripts of 1844* brought to the world exactly what revolutionary theory is
supposed to bring: *more light*.

Early Marx was no Marxist, and never even had to pronounce himself on the
matter, for Marxism hadn't been invented yet. Late Marx was no Marxist, either,
and said so himself, more than once. Lukewarm liberals and ex-radicals galore
have genuflected endlessly on Marx's jocular disclaimer, in vain attempts to
convince themselves and the gullible that the author of *The Civil War in
France* wound up on the side of the faint-hearted. But when Marx declared "I am
no Marxist" he was certainly not renouncing his life's work or his
revolutionary passion. He was rejecting the reification and caricature of his
work by "disciples" who preferred the study of scripture to the study of life,
and mistook the quoting of chapter and verse and slogan for revolutionary
theory and practice. Unlike these and legions of later "Marxists," Marx refused
to evaluate a constantly changing reality by means of exegeses of his own
writings. For him, the study of texts---and he was a voracious reader if ever
there was one---was part of a process of self-clarification and
self-correction, a testing of his views against the arguments and evidence of
others, a broadening of perspectives through an ongoing and open confrontation
with the new and unexpected. For Late Marx, the motto *doubt everything* was no
joke. Or at least it was not *only* a joke.

This is especially noticeable in the last decade of Marx's life, and the
*Ethnological Notebooks* are an especially revealing example of his readiness
to revise previously held views in the light of new discoveries. At the very
moment that his Russian "disciples"---those "admirers of capitalism," as he
ironically tagged them---were loudly proclaiming that the laws of historical
development set forth in the first volume of *Capital* were universally
mandatory, Marx himself was diving headlong into the study of (for him) *new
experiences of resistance and revolt against oppression*---by North American
Indians, Australian aborigines, Egyptians and Russian peasants. As we have
seen, this study led him not only to dramatically and extensively alter his
earlier views, but also to champion a movement in Russia that his "disciples"
there and elsewhere scorned as "ahistorical," "utopian," "unrealistic" and
"petty-bourgeois." Even today such epithets are not unfamiliar to anyone who
has ever dared to struggle against the existing order in a manner unprescribed
by the "Marxist" Code of Law.

Late Marx also undercuts the several neo- and anti-Marxisms that have, from
time to time, held the spotlight in the intellectual fashion shows of recent
years---those hothouse hybrids concocted by specialists who seem to have
persuaded themselves that they have gone "beyond Marx" by modifying his
revolutionary project of "merciless criticism of everything in existence" into
one or another specifically academic program of inoffensively mild and
superficial criticism, not of everything, but only of whatever happens to fall
within the four walls of their particular compartmentalized specialty. Not
surprisingly, when the advocates of these neo-Marxisms finally get around to
adopting a political position, it tends to be incurably reformist. Their sad
fate in this regard serves to remind us that it is not by being less merciless
in our criticism, less rigorous in our research, or less revolutionary in our
social activity that we are likely to go beyond Marx. Despite their pompous
claims, ninety-seven percent of the neo-Marxists are actually to the right of
the crude and mechanical Marxists of the old sects, and the separation of their
theory from their practice tends to be much larger. Certainly the Wobbly hobo
of yesteryear, whose Marxist library consisted of little more than the IWW
Preamble and the *Little Red Song Book*, had a far surer grasp of social
reality---and indeed, of what Marx and even Hegel were talking about---than
today's professional phenomenologist-deconstructionist neo-Marxologist who, in
addition to writing unreadable micro-analytical explications of Antonio
Gramsci, insists on living in an all-white neighborhood, crosses the university
clerical workers' picket line, and votes the straight Democratic ticket.

There is every reason to believe that "Late Marx," and the *Ethnological
Notebooks* in particular, will provide for the next global revolutionary wave
something of the illumination that Early Marx brought in the '60s. By helping
to finish off what remains of the debilitating hegemony of the various
"Marxist" orthodoxies a well as the evasive and confusional pretensions of the
various "neo-Marxisms," Late Marx will contribute to a new flowering of
*audacity, audacity and still more audacity* that alone defines the terms of
revolutionary theory and practice.

Late Marx emphasized as never before the *subjective factor* as the decisive
force in revolution. His conclusion that revolutionary social transformation
could proceed from different directions and in different (though not
incompatible) ways was a logical extension of his multilinear view of history
into the present and future. This new pluralism turned out to be emphatically
anti-reformist, however, and it is pleasant to discover that the proponents of
gradualism, nationalization, Eurocommunism, social democracy, "liberation
theology" and other sickeningly sentimental and fundamentally bourgeois
aberrations will find no solace in Late Marx. On the contrary, the
*Ethnological Notebooks* and Marx's other writings of the last period develop
both the fierce *anti-statism* that became a prime focus of his work after the
Paris Commune, and the merciless *critique of religion* that had provided the
groundwork of his writings of 1843--45. Late Marx did not become an anarchist,
but his last writings establish a firm basis for the historical reconciliation
of revolutionary Marxists and anarchists that André Breton called for in his
*Légitime Défense* in 1926.

Pivotal to all the excitement, playfulness, humor, discovery and diversity of
Late Marx---so reminiscent of the *mood* of the 1844 texts---his
anthropological investigations have a special relevance for today. If a century
later, Marx's "return to the projects of his Paris youth" still glows brightly
with the colors of the future, it is because the possibilities of the
revolutionary strategy suggested in these notebooks and related writings are
far from being exhausted.

A gathering of the loose ends of a lifetime of revolutionary thought and
action, the *Ethnological Notebooks* embody the final deepening and expanding
of Marx's historical perspectives, and therefore of his perspectives for
revolution, by Marx himself. They are, in a sense, the last will and testament
of Marx's own Marxism. In these notes the "philosophical anthropology" of 1844
is empirically filled in, made more concrete, theoretically rounded out and in
the end qualitatively transformed for, as Hegel observed in the
*Phenomenology*, "in the alteration of the knowledge, the object itself also
... is altered."

Fragmentary though they are, the *Notebooks*, together with the drafts of the
letter to Vera Zasulich and a few other texts, reveal that Marx's culminating
revolutionary vision is not only coherent and unified, but a ringing challenge
to all the manifold Marxisms that still try to dominate the discussion of
social change today, and to all truly revolutionary thought, all thought
focused on the reconciliation of humankind and the planet we live on. In this
challenge lies the greatest importance of these texts. A close, critical look
back to the rise and fall of ancient pre-capitalist communities, Marx's
*Ethnological Notebooks* and his other last writings also look ahead to today's
most promising revolutionary movements in the Third World, and the Fourth, and
our own.

Raya Dunayevskaya, to whom we owe the best that has been written on the
*Notebooks*, rightly pointed out that "there is no way for us to know what Marx
intended to do with this intensive study." One need not be a card-carrying
prophet to know in advance that this undeveloped work on underdeveloped
societies will be developed in many different ways in the coming years.

But here is something to think about, tonight and tomorrow: With his radical
new focus on the primal peoples of the world; his heightened critique of
civilization and its values and institutions; his new emphasis on the
subjective factor in revolution; his ever-deeper hostility to religion and
State; his unequivocal affirmation of revolutionary pluralism; his growing
sense of the unprecedented depth and scope of the communist revolution as a
*total* revolution, vastly exceeding the categories of economics and politics;
his bold new posing of such fundamental questions as the relation of man and
woman, humankind and nature, imagination and culture, myth and ritual and all
the "passions and Powers of the mind." Late Marx is sharply opposed to, *and
incomparably more radical than,* almost all that we know today as Marxism. At
the same time---and everyone who understands Blake and Lautréamont and
Thelonious Monk will know that this is no mere coincidence---Marx's culminating
synthesis is very close to the point of departure of *surrealism*, the
"communism of genius."
