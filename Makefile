SOURCES := $(wildcard *.md)
TARGETS := $(patsubst %.md,%.html,$(SOURCES))

STYLES := tufte-css/tufte.css \
	pandoc.css \
	tufte-extra.css \
	asterism.css

FILTERS := pandoc-sidenote

.PHONY: all
all: $(TARGETS)

## Generalized rule: how to build a .html file from each .md
%.html: %.md tufte.html5 $(STYLES)
	pandoc \
	    --self-contained \
	    --html-q-tags \
	    --ascii \
	    --to html5+smart \
	    --template=tufte.html5 \
	    $(foreach filter,$(FILTERS),--filter $(filter)) \
	    $(foreach style,$(STYLES),--css $(style)) \
	    $< \
	    -o $@
